package tk.alex4ip.catalog.dao;

import java.math.BigDecimal;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.websystique.springmvc.model.Category;


public class CategoryDAOImplTest extends EntityDaoImplTest{

	@Autowired
	CategoryDAO categoryDAO;

	@Override
	protected IDataSet getDataSet() throws Exception{
		IDataSet dataSet = new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Category.xml"));
		return dataSet;
	}
	
	/* In case you need multiple datasets (mapping different tables) and you do prefer to keep them in separate XML's
	@Override
	protected IDataSet getDataSet() throws Exception {
	  IDataSet[] datasets = new IDataSet[] {
			  new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Category.xml")),
			  new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Benefits.xml")),
			  new FlatXmlDataSet(this.getClass().getClassLoader().getResourceAsStream("Departements.xml"))
	  };
	  return new CompositeDataSet(datasets);
	}
	*/

	@Test
	public void findById(){
		Assert.assertNotNull(categoryDAO.findById(1));
		Assert.assertNull(categoryDAO.findById(3));
	}

	
	@Test
	public void saveCategory(){
		categoryDAO.saveCategory(getSampleCategory());
		Assert.assertEquals(categoryDAO.findAllCategories().size(), 3);
	}
	
	@Test
	public void deleteCategoryBySsn(){
		categoryDAO.deleteCategoryBySsn("11111");
		Assert.assertEquals(categoryDAO.findAllCategories().size(), 1);
	}
	
	@Test
	public void deleteCategoryByInvalidSsn(){
		categoryDAO.deleteCategoryBySsn("23423");
		Assert.assertEquals(categoryDAO.findAllCategories().size(), 2);
	}

	@Test
	public void findAllCategories(){
		Assert.assertEquals(categoryDAO.findAllCategories().size(), 2);
	}
	
	@Test
	public void findCategoryBySsn(){
		Assert.assertNotNull(categoryDAO.findCategoryBySsn("11111"));
		Assert.assertNull(categoryDAO.findCategoryBySsn("14545"));
	}

	public Category getSampleCategory(){
		Category category = new Category();
		category.setName("Karen");
		category.setSsn("12345");
		category.setSalary(new BigDecimal(10980));
		category.setJoiningDate(new LocalDate());
		return category;
	}

}
