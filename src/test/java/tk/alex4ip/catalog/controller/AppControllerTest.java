package tk.alex4ip.catalog.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.atLeastOnce;

import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.websystique.springmvc.model.Category;
import com.websystique.springmvc.service.CategoryService;

public class AppControllerTest {

	@Mock
	CategoryService service;
	
	@Mock
	MessageSource message;
	
	@InjectMocks
	AppController appController;
	
	@Spy
	List<Category> categories = new ArrayList<Category>();

	@Spy
	ModelMap model;
	
	@Mock
	BindingResult result;
	
	@BeforeClass
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		categories = getCategoryList();
	}
	
	@Test
	public void listCategories(){
		when(service.findAllCategories()).thenReturn(categories);
		Assert.assertEquals(appController.listCategories(model), "allcategories");
		Assert.assertEquals(model.get("categories"), categories);
		verify(service, atLeastOnce()).findAllCategories();
	}
	
	@Test
	public void newCategory(){
		Assert.assertEquals(appController.newCategory(model), "registration");
		Assert.assertNotNull(model.get("category"));
		Assert.assertFalse((Boolean)model.get("edit"));
		Assert.assertEquals(((Category)model.get("category")).getId(), 0);
	}


	@Test
	public void saveCategoryWithValidationError(){
		when(result.hasErrors()).thenReturn(true);
		doNothing().when(service).saveCategory(any(Category.class));
		Assert.assertEquals(appController.saveCategory(categories.get(0), result, model), "registration");
	}

	@Test
	public void saveCategoryWithValidationErrorNonUniqueSSN(){
		when(result.hasErrors()).thenReturn(false);
		when(service.isCategorySsnUnique(anyInt(), anyString())).thenReturn(false);
		Assert.assertEquals(appController.saveCategory(categories.get(0), result, model), "registration");
	}

	
	@Test
	public void saveCategoryWithSuccess(){
		when(result.hasErrors()).thenReturn(false);
		when(service.isCategorySsnUnique(anyInt(), anyString())).thenReturn(true);
		doNothing().when(service).saveCategory(any(Category.class));
		Assert.assertEquals(appController.saveCategory(categories.get(0), result, model), "success");
		Assert.assertEquals(model.get("success"), "Category Axel registered successfully");
	}

	@Test
	public void editCategory(){
		Category emp = categories.get(0);
		when(service.findCategoryBySsn(anyString())).thenReturn(emp);
		Assert.assertEquals(appController.editCategory(anyString(), model), "registration");
		Assert.assertNotNull(model.get("category"));
		Assert.assertTrue((Boolean)model.get("edit"));
		Assert.assertEquals(((Category)model.get("category")).getId(), 1);
	}

	@Test
	public void updateCategoryWithValidationError(){
		when(result.hasErrors()).thenReturn(true);
		doNothing().when(service).updateCategory(any(Category.class));
		Assert.assertEquals(appController.updateCategory(categories.get(0), result, model,""), "registration");
	}

	@Test
	public void updateCategoryWithValidationErrorNonUniqueSSN(){
		when(result.hasErrors()).thenReturn(false);
		when(service.isCategorySsnUnique(anyInt(), anyString())).thenReturn(false);
		Assert.assertEquals(appController.updateCategory(categories.get(0), result, model,""), "registration");
	}

	@Test
	public void updateCategoryWithSuccess(){
		when(result.hasErrors()).thenReturn(false);
		when(service.isCategorySsnUnique(anyInt(), anyString())).thenReturn(true);
		doNothing().when(service).updateCategory(any(Category.class));
		Assert.assertEquals(appController.updateCategory(categories.get(0), result, model, ""), "success");
		Assert.assertEquals(model.get("success"), "Category Axel updated successfully");
	}
	
	
	@Test
	public void deleteCategory(){
		doNothing().when(service).deleteCategoryBySsn(anyString());
		Assert.assertEquals(appController.deleteCategory("123"), "redirect:/list");
	}

	public List<Category> getCategoryList(){
		Category e1 = new Category();
		e1.setId(1);
		e1.setName("Axel");
		e1.setJoiningDate(new LocalDate());
		e1.setSalary(new BigDecimal(10000));
		e1.setSsn("XXX111");
		
		Category e2 = new Category();
		e2.setId(2);
		e2.setName("Jeremy");
		e2.setJoiningDate(new LocalDate());
		e2.setSalary(new BigDecimal(20000));
		e2.setSsn("XXX222");
		
		categories.add(e1);
		categories.add(e2);
		return categories;
	}
}
