<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Item details</title>
</head>
<body>
	<jsp:include flush="false" page="menu.jsp" />

	<h2>Edit '${item.name}' item #${item.id} from ${item.category} category</h2>
	<form:form method="POST" modelAttribute="item">
		<form:input type="hidden" path="id" id="id"/>
		<table>
			<tr>
				<td><label for="name">Name: </label> </td>
				<td><form:input path="name" id="name"/></td>
			<tr>
			<tr>
				<form:select path="category.id">
            	<form:option value="0" label="Select" />
            	<form:options items="${categories}" itemValue="id" itemLabel="name" />
            </form:select>
			<tr>			
			
			
				<td colspan="3">
					<c:choose>
						<c:when test="${edit}">
							<input type="submit" value="Update"/>
						</c:when>
						<c:otherwise>
							<input type="submit" value="Register"/>
						</c:otherwise>
					</c:choose>
				</td>
			</tr>
		</table>
	</form:form>

	<a href="${pageContext.request.contextPath}/i/"
		title="Item List">Item List</a>
		
		<jsp:include flush="false" page="footer.jsp" />
		
</body>
</html>