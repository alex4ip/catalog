package tk.alex4ip.catalog.dao;

import java.util.List;

import tk.alex4ip.catalog.model.Item;

public interface ItemDAO {
	
	public Item findById(int id);
	
	public void saveItem(Item item);
	
	public void deleteItem(int id);
	
	public List<Item> findAllItems();	
	
	public List<Item> findItemsByName(String name);

}
