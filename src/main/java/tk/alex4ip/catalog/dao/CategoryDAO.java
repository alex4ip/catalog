package tk.alex4ip.catalog.dao;

import java.util.List;

import tk.alex4ip.catalog.model.Category;

public interface CategoryDAO {
	
	public Category findById(int id);
	
	public void saveCategory(Category category);
	
	public void deleteCategory(int id);
	
	public List<Category> findAllCategories();

}
