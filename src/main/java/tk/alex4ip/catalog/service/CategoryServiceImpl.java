package tk.alex4ip.catalog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tk.alex4ip.catalog.dao.CategoryDAO;
import tk.alex4ip.catalog.model.Category;

@Service("categoryService")
@Transactional 
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryDAO categoryDAO;

	public Category findById(int id) {
		return categoryDAO.findById(id);
	}

	public void saveCategory(Category category) {
		 categoryDAO.saveCategory(category);
	}
	
	public void updateCategory(Category category) {
		Category entity = categoryDAO.findById(category.getId());
		if(entity!=null){
			entity.setName(category.getName());
		}
	}
	
	public void deleteCategory(int id) {
		categoryDAO.deleteCategory(id);
	}
	
	public List<Category> findAllCategories() {
		return categoryDAO.findAllCategories();
	}	

}
	
